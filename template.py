#!/usr/local/bin/python3

'''This is a template Python file that can be used to connect to
Cytoscape using the py2cytoscape plugin for simulating purposes.

-Expand documentation here to explain its usage-
'''

from py2cytoscape.data.cyrest_client import CyRestClient
from MoleculeFactory import MoleculeFactory
import requests
import time
import sys


__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2016.1"

'''
Global Variables

The network view object is set after creating the network and
allows access to the 'view' used for getting and setting object
properties.'''
_NETWORK_VIEW = None

def connect():
    '''Connects to Cytoscape using the cyREST API. The returned
    Cytoscape' object can be called to create networks and change
    network layouts and styles.

    Returns:
        a Cytoscape object handle'''
    server = CyRestClient()
    return server


def create_network(handle, network_name):
    '''Creates the network given the 'network_name' in Cytoscape and
    returns its ID given by Cytoscape. Also sets the global 'view'
    object used by the get/set-property functions

    Args:
        handle: the Cytoscape client object
        network_name: the preferred name for the new network

    Returns:
        A network object with methods for adding nodes and edges
        and access to the network 'view' object used for altering
        the properties of the objects in the network
    '''
    global _NETWORK_VIEW

    # Delete any existing network
    handle.network.delete_all()

    # Create network, store its ID
    network = handle.network.create(name=network_name)

    # Set the network 'view' object
    _NETWORK_VIEW = network.get_view(network.get_views()[0],
                                     format='view')
    # Return the network ID
    return network


def draw_static_nodes(handle):
    '''Use this function to draw/initialize all the static nodes in your simulation.
    For example membrane lipids or passive transport proteins.

    Args:
        handle: the network handle used for creating objects and
                modifying their properties

    Returns:
        a dictionary with stored molecules
    '''

    '''The rest of this function demonstrates the use of the 'MoleculeFactory' that can be used in the
    citric acid cycle simulation.'''

    aconitate = MoleculeFactory(handle, 'cis-Aconitate')
    aconitate.build_molecule(20.0, -200.0)

    citrate = MoleculeFactory(handle, 'Citrate')
    citrate.build_molecule(-300.0, 200.0)

    ketoglutarate = MoleculeFactory(handle, 'a-ketoglutarate')
    ketoglutarate.build_molecule(350.0, 175.0)

    succinate = MoleculeFactory(handle, 'Succinate')
    succinate.build_molecule(-100.0, 50.0)

    '''Nodes and MoleculeFactories are simple Python objects which we can store in any Python data type for later use.
    The following code demonstrates creating two molecules and storing them in a dictionary and they are later
    used in the simulation process.'''

    molecules = {}

    # Storing the molecule object in the dictionary
    molecules['succinyl'] = MoleculeFactory(handle, 'Succinyl-CoA')
    # Calling a method of the stored molecule
    molecules['succinyl'].build_molecule(400.0, 0.0)

    molecules['isocitrate'] = MoleculeFactory(handle, 'D-Isocitrate')
    molecules['isocitrate'].build_molecule(-50.0, 350.0)

    # Returning the molecules
    return molecules

def simulate(handle, molecules):
    '''This function is used for all dynamic content of the simulation.

    Args:
        handle: the network handle used for creating objects and
                modifying their properties
        molecules: a dictionary object holding molecules used in the simulation
    '''

    FPS = 60
    travel_time = 3
    # succinyl
    x1delta = -600
    y1delta = -110
    # isocitrate
    x2delta = 300
    y2delta = -400
    frames = FPS * travel_time

    ''' Since the simulation appears to be going rather slow, we time the execution of the simulation
    to calculate the actual frame time and FPS.'''
    start = time.time()

    # Start the simulation
    for i in range(frames):
        # Disable the wobble effect for the final frame to get the atoms aligned properly
        if i < (frames - 1):
            molecules['succinyl'].move_position(x1delta / frames, y1delta / frames)
            molecules['isocitrate'].move_position(x2delta / frames, y2delta / frames)
        else:
            molecules['succinyl'].move_position(x1delta / frames, y1delta / frames, wobble=False)
            molecules['isocitrate'].move_position(x2delta / frames, y2delta / frames, wobble=False)
        time.sleep(0.02)#travel_time / FPS)

    # Measure total time taken
    time_taken = time.time() - start

    # Just print some statistics
    print('Total time: ', round(time_taken, 3), 's\nFrame time: ', round(time_taken / (FPS * travel_time), 3),
          's\nFPS: ', round(1 / (time_taken / (FPS * travel_time)), 3), sep='')


def get_node_property(node_suid, node_property):
    '''Gets the value for the visual-style property of the node using
    the global 'view' object

    Args:
        node_suid: The SUID of the node
        node_property: The name of the visual-property data to get

    Returns:
        The value for the corresponding visual-property
    '''
    # Get a dictionary with all properties of all nodes
    node_view_dict = _NETWORK_VIEW.get_node_views_as_dict()
    # Get the properties for the requested node
    node_properties = node_view_dict[node_suid]
    # Return the requested property value
    return node_properties[node_property]


def set_node_property(node_suid, node_property, value):
    '''Sets the visual-style property of the given node to the
    given value

    Args:
        node_suid: The SUID of the node
        node_property: The name of the visual-property data to get
        value: The value to which to set the property
    '''
    _NETWORK_VIEW.update_node_views(visual_property=node_property,
                                    values={node_suid: value})


def set_nodes_property(node_suids, node_property, value):
    '''Sets the visual-style property of multiple nodes to the given
    value

    Args:
        node_suid: The SUID of the node
        node_property: The name of the visual-property data to set
        value: The desired property value
    '''
    # Create a new dictionary with node_suid:'value' for all nodes
    new_values = {}
    for suid in node_suids:
        new_values[suid] = value
    # Apply the view
    _NETWORK_VIEW.update_node_views(visual_property=node_property,
                                    values=new_values)

def remove_node(network_id, node_suid):
    ''' Removes the node by performing a CyREST DELETE request'''
    # Base REST API V1 URI
    base_url = 'http://localhost:1234/v1/networks/'
    requests.delete(base_url + str(network_id) + "/nodes/" + str(node_suid) + "/")

def main():
    '''Connect to Cytoscape and execute the simulation steps in order'''
    cytoscape = connect()
    network = create_network(cytoscape, 'MyFirstNetwork')
    molecules = draw_static_nodes(network)

    # Sleep for a few seconds, this time can be used to setup Cytoscape properly (i.e. maximize window etc)
    time.sleep(20)

    # Start the simulation
    simulate(network, molecules)
    return 0

if __name__ == "__main__":
    '''Execute the main function if this program is executed commandline'''
    sys.exit(main())
