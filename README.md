# A simple py2cytoscape template used for running simulations in Cytoscape #

This project contains an updated template extended with a *Molecule Factory* that builds the metabolites as seen in the citric acid cycle.

To use the template:

* Download the project using the *Downloads* link in the left-menu
* Extract the downloaded ZIP file
* Start Cytoscape (version >= 3.3.0 preferred)
* Create a new *visual style* (**Style** menu in the Control Panel, *Create new style* and name it as you want)
* Run the **template.py** file: `python3 template.py`

The following molecules can be rendered using the **MoleculeFactory**:

* Citrate
* cis-Aconitate
* D-Isocitrate
* Oxalosuccinic acid
* a-ketoglutarate
* Succinyl-CoA
* Ccetyl-CoA
* Succinate
* Fumarate
* L-Malate
* Oxaloacetate
* H2O
* CO2
* NAD+
* NADH
* FAD+
* FADH2
* GDP
* GTP
* Co-A-S-H
* ADP
* ATP
* Ubiquinone
* Ubiquinol
* Phosphorus

The following network will appear in Cytoscape when running the **template**:

* With wobble:
    * ![Demo network](https://bitbucket.org/mkempenaar/py2cytoscape/raw/master/img/demo.gif)
* Without wobble:
    * ![Demo network](https://bitbucket.org/mkempenaar/py2cytoscape/raw/master/img/demo_no_wobble.gif) 

