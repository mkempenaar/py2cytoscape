'''
Cytoscape Edge object creator
'''

__author__ = 'Marcel Kempenaar'


class Edge(object):
    '''
    Edge object between to nodes. Requires the network in which to create the edge, two
    Node objects to connect and a string identifier for the interaction.
    '''

    def __init__(self, network, source_node, target_node, interaction,
                 line_type='', width=1, color='black'):
        self.network = network
        self.source_node = source_node
        self.target_node = target_node
        self.interaction = interaction
        self.line_type = line_type
        self.width = width
        self.color = color
        self.view = None
        self.suid = 0
        self._init_edge()

    def _init_edge(self):
        '''
        Creates an edge given the source node, target node and named interaction
        '''
        edge = self.network.add_edge(self.source_node.get_suid(),
                                     self.target_node.get_suid(),
                                     self.interaction)
        self.suid = edge[0]['SUID']
        self.view = self.network.get_view(self.network.get_views()[0], format='view')

    def set_edge_property(self, edge_property, value):
        '''
        Sets the given property to the given value
        :param edge_property: a valid Edge property
        :param value: a valid value for the given property
        '''
        if self.view is not None:
            self.view.update_edge_views(visual_property=edge_property,
                                        values={self.suid, value})

    def get_suid(self):
        '''
        Returns the SUID for the created edge
        :return: a SUID (int)
        '''
        return self.suid
