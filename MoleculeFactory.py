'''
Contains a class for building simple molecules using a template
'''

from Node import *
from Edge import Edge
import random
import requests

__author__ = 'Marcel Kempenaar'


class MoleculeFactory():
    ''' Creates a molecule from one of the templated molecules
    '''
    def __init__(self, network, molecule):
        '''
            Molecule template and atom-positions:

            level:    positions:

            +2             13   14
                             \ /
            +1        7   8   9  10  11  12
                      |   |   |   |   |   |
            0         0 - 1 - 2 - 3 - 4 - 5 - 6
                      |   |   |   |   |   |
            -1        15  16  17  18  19  20

            Available atoms that can be filled in:
                Oxygen (O), Carbon (C), Sulfur (S), Coa (A)
        '''
        self.atoms = ['O', 'C', 'S', 'a', 'A', 'H', 'OH', 'G', 'R', 'p', 'n', 'f']
        self.molecules = {         # template position: 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0
                'Citrate':         ['C6H8O7',     'C C C C C - - O2 H C H O2 - O2 OH OH H OH H OH -', 2],
                'cis-Aconitate':   ['C6H6O6',     'C C C C2 C - - O2 H C - O2 - O2 OH OH H - H OH -', 2],
                'D-Isocitrate':    ['C6H8O7',     'C C C C C - - O2 H C H O2 - O2 OH OH H H OH OH -', 2],
                "Oxalosuccinic acid":[None,       'C C C C C - - O2 H C - O2 - O2 OH OH H H O2 OH -', 2],
                'a-ketoglutarate': ['C5H6O5',     'C C C C C - - O2 H H - O2 - - - OH H H O2 OH -', 2],
                'Succinyl-CoA':    ['C4H5O3S1A1', 'C C C C S a - O2 H H - - - - - O H H O2 - -', 3],
                "Ccetyl-CoA":      ["C2H3OS-coa", 'H C C S a - - - H - - - - - - - H O2 - - -', 2],
                'Succinate':       ['C4H6O4',     'C C C C - - - O2 H H OH - - - - OH H H O2 - -', 2],
                'Fumarate':        ['C4H4O4',     'C C C2 C - - - O2 H - OH - - - - OH - H O2 - -', 2],
                'L-Malate':        ['C4H6O5',     'C C C C - - - OH H H O2 - - - - O2 H OH OH - -', 2],
                'Oxaloacetate':    ['C4H4O5',     'C C C C - - - OH - H O2 - - - - O2 O2 H OH - -', 2],
                "H2O":             ["H2O",        '- - - - - - - - - O - - - H H - - - - - -', 2],
                "CO2":             ["CO2",        'O C2 O2 - - - - - - - - - - - - - - - - -' , 1],
                "NAD+":            [None,         'n - - - - - - - - - - - - - - - - - - - -', 0],
                "NADH":            ["NAD-H",      'n H - - - - - - - - - - - - - - - - - - -', 0],
                "FAD+":            [None,         'f - - - - - - - - - - - - - - - - - - - -', 0],
                "FADH2":           ["NAD-H",      'f H H - - - - - - - - - - - - - - - - - -', 1],
                "GDP":             ["Gpp",        'G R P P - - - - - - - - - - - - - - - - -', 1],
                "GTP":             ["Gppp",       'G R P P P - - - - - - - - - - - - - - - -', 2],
                "Co-A-S-H":        [None,         'a S H - - - - - - - - - - - - - - - - - -', 1],
                "ADP":             ["App",        'A R P P - - - - - - - - - - - - - - - - -', 1],
                "ATP":             ["Appp",       'A R P P P - - - - - - - - - - - - - - - -', 2],
                "Ubiquinone":      ["Q",          'Q - - - - - - - - - - - - - - - - - - - -', 0],
                "Ubiquinol":       ["QH2",        'Q H H   - - - - - - - - - - - - - - - - -', 1],
                "Phosphorus":      ["PO4 3-",     'P - -   - - - - - - - - - - - - - - - - -', 0],
                }
        self.atom_size = 20
        self.atom_spacing = 40
        self.network = network
        self.xpos, self.ypos = (0.0, 0.0)
        self.view = None
        self.nodes = []
        self.edges = []
        self.build = False
        self.mol = self.molecules[molecule]
        self._set_atom_positions()
    
    def remove_molecule(self):
        ''' Removes the molecul by performing a CyREST DELETE request for all node SUIDs '''
        network_id = self.network.get_id()
        # Base REST API V1 URI
        base_url = 'http://localhost:1234/v1/networks/'

        for node in self.nodes + self.extra_hydrogens:
            if node is not None:
                requests.delete(
                    base_url + str(network_id) + "/nodes/" + str(node.get_suid()) + "/")

    def _set_atom_positions(self):
        '''
        Calculates the positions for all nodes in the template. These stored positions
        are later used to place the nodes when the molecule is built.
        '''
        self.base_positions = {}
        offset = -(self.mol[2] * self.atom_spacing)
        
        # atom base_positions 0 - 6
        startpos = (0.0 + offset, 0.0)
        self.base_positions[1] = []
        delta = (self.atom_spacing, 0.0)
        for i in range(7):
            self.base_positions[1].append(tuple(startpos[idx] + (i * delta[idx]) for idx in range(2)))

        # atom base_positions 7 - 12
        startpos = (0.0 + offset, -self.atom_spacing)
        self.base_positions[2] = []
        for i in range(6):
            self.base_positions[2].append(tuple(startpos[idx] + (i * delta[idx]) for idx in range(2)))

        # atom base_positions 13, 14, with position 9 as base
        startpos = self.base_positions[2][2]
        self.base_positions[3] = [
            # Position 13
            (startpos[0] - (self.atom_spacing/2),
             startpos[1] - (self.atom_spacing/2)),
            # Position 14
            (startpos[0] + (self.atom_spacing/2),
             startpos[1] - (self.atom_spacing/2))]

        # atom base_positions 15 - 20
        startpos = (0.0 + offset, self.atom_spacing)
        self.base_positions[4] = []
        for i in range(6):
            self.base_positions[4].append(tuple(startpos[idx] + (i * delta[idx]) for idx in range(2)))

    def build_molecule(self, xpos, ypos):
        '''
        Constructs the molecule given its template in layers and positions all
        atoms based on the given x- and y-coordinate
        :param xpos: x-position for node in position 0
        :param ypos: y-position for node in position 0
        '''
        # Remove the molecule if it already exists
        if self.build:
            self.remove_molecule()
        
        atoms = self.mol[1].split()
        self.xpos = xpos
        self.ypos = ypos

        # Adding the nodes, each layer separate.
        self._add_atoms(atoms[:7], self.base_positions[1])
        self._add_atoms(atoms[7:13], self.base_positions[2])
        self._add_atoms(atoms[13:15], self.base_positions[3])
        self._add_atoms(atoms[15:20], self.base_positions[4])

        # Adding the edges
        self._define_edges(atoms)

        # Getting the view
        self.view = self.network.get_view(self.network.get_views()[0], format='view')
        
        # Set the 'build' flag
        self.build = True

    def _add_atoms(self, atoms, positions):
        '''
        Adds a node for all atoms in the molecule
        :param atoms: template list of molecules to add for a single layer
        :param positions: coordinates for placing the atoms
        '''
        for i in range(len(atoms)):
            xpos, ypos = positions[i]
            xpos += self.xpos
            ypos += self.ypos
            if atoms[i][0] == 'C':
                self.nodes.append(Carbon(self.network, self.atom_size, 'C', xpos, ypos))
            elif atoms[i] == 'OH':
                self.nodes.append(Hydroxide(self.network, self.atom_size, 'OH', xpos, ypos))
            elif atoms[i][0] == 'O':
                self.nodes.append(Oxygen(self.network, self.atom_size, 'O', xpos, ypos))
            elif atoms[i][0] == 'S':
                self.nodes.append(Sulfur(self.network, self.atom_size, 'S', xpos, ypos))
            elif atoms[i][0] == 'a':
                self.nodes.append(CoenzymeA(self.network, self.atom_size, 'CoA', xpos, ypos))
            elif atoms[i][0] == 'H':
                self.nodes.append(Hydrogen(self.network, self.atom_size, 'H', xpos, ypos))
            elif atoms[i][0] == "P":
                self.nodes.append(Phosphorus(self.network, self.atom_size, "P", xpos, ypos))
            elif atoms[i][0] == "G":
                self.nodes.append(Guanine(self.network, self.atom_size, "Guanine", xpos, ypos))
            elif atoms[i][0] == "R":
                self.nodes.append(Ribose(self.network, self.atom_size, "Ribose", xpos, ypos))
            elif atoms[i][0] == "n":
                self.nodes.append(NAD(self.network, self.atom_size, "NAD+", xpos, ypos))
            elif atoms[i][0] == "f":
                self.nodes.append(FAD(self.network, self.atom_size, "FAD+", xpos, ypos))
            elif atoms[i][0] == "A":
                self.nodes.append(Adenine(self.network, self.atom_size, "Adenine", xpos, ypos))
            elif atoms[i][0] == "Q":
                self.nodes.append(Ubiquinone(self.network, self.atom_size, "Ubiquinone", xpos, ypos))
            else:
                self.nodes.append(None)

    def _define_edges(self, atoms):
        '''
        Defines the edges by specifying the layers and their positional difference as an
        offset. Calls the method that creates the edges between present atoms
        :param atoms: the template list of atoms
        '''
        # level 0 (backbone)
        self._add_edges(6, atoms, 1)
        # levels 0 and +1
        self._add_edges(6, atoms, 7)
        # levels 0 and -1
        self._add_edges(6, atoms, 15)

        # levels +2 and +3 (position 9, 13 and 14
        for i in [13, 14]:
            if atoms[i] is not '-' and atoms[9] is not '-':
                if len(atoms[i]) > 1 and atoms[i] != "OH":
                    bonds = int(atoms[i][1:])
                else:
                    bonds = 1
                for n in range(bonds):
                    self.edges.append(Edge(self.network, self.nodes[i], self.nodes[9], 'bond'))

    def _add_edges(self, n_atoms, atoms, offset):
        '''
        Adds edges between two layers of nodes.
        Checks if both nodes (defined by their offset) are not '-', i.e. contain an atom
        and creates an edge between these nodes
        :param n_atoms: number of atoms in the layer
        :param atoms: the template list of atoms
        :param offset: positional difference between nodes in the layers
        '''
        for i in range(n_atoms):
            if atoms[i] is not '-' and atoms[i+offset] is not '-':
                if len(atoms[i+offset]) > 1 and atoms[i+offset] != "OH":
                    bonds = int(atoms[i+offset][-1])
                else:
                    bonds = 1
                for n in range(bonds):
                    self.edges.append(Edge(self.network,
                                           self.nodes[i],
                                           self.nodes[i+offset], 'bond'))

    def move_position(self, xpos, ypos, wobble=True):
        '''Moves all nodes with the given coordinates
        :param xpos: amount of pixels to move the molecule on the x-axis
        :param ypos: amount of pixels to move the molecule on the y-axis
        :param wobble: introduces a random offset to simulate a 'natural wobble' effect
        '''
        new_node_x_positions = {}
        new_node_y_positions = {}
        for node in self.nodes:
            # Generate random wobble picked from a normal distribuion (m=0, sd=2)
            if wobble:
                rand_wobble = random.normalvariate(0, 2)
            else:
                rand_wobble = 0.0
            if node is not None and node.name != "OH":
                # Update node positions (does not change the actual position)
                node.xpos += xpos
                node.ypos += ypos
                new_node_x_positions[node.get_suid()] = (node.xpos + rand_wobble)
                new_node_y_positions[node.get_suid()] = (node.ypos - rand_wobble)
            elif node and node.name == "OH":
                # Moves the nodes that make up the O-H atoms
                node.xpos += xpos
                node.ypos += ypos
                node.hydrogen.xpos += xpos
                node.hydrogen.ypos += ypos
                
                new_node_x_positions[node.get_suid()] = (node.xpos + rand_wobble)
                new_node_y_positions[node.get_suid()] = (node.ypos - rand_wobble)
                
                new_node_x_positions[node.hydrogen.get_suid()] = (node.hydrogen.xpos + rand_wobble)
                new_node_y_positions[node.hydrogen.get_suid()] = (node.hydrogen.ypos - rand_wobble)

       
        # Moves the nodes to the new position
        self.view.update_node_views(visual_property='NODE_X_LOCATION',
                                    values=new_node_x_positions)
        self.view.update_node_views(visual_property='NODE_Y_LOCATION',
                                    values=new_node_y_positions)
