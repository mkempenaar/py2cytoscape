'''
The Node class creates a basic Node in Cytoscape
'''

__author__ = 'Marcel Kempenaar'


class Node(object):
    """Creates a single node in a given network"""
    def __init__(self, network, name, shape='Octagon', color='purple', size=40):
        self.network = network
        self.name = name
        self.shape = shape
        self.color = color
        self.size = size
        self.xpos = 0.0
        self.ypos = 0.0
        self.zpos = 0.0
        self.suid = 0
        self.view = None
        self.init_node()

    def init_node(self):
        '''
        Creates the node and sets some basic properties
        '''
        # Add node to the network
        node = self.network.add_node(self.name)
        self.suid = node.get(self.name)

        # Get the view
        self.view = self.network.get_view(self.network.get_views()[0], format='view')

        # Set node properties
        self.set_property('NODE_SHAPE', self.shape)
        self.set_property('NODE_FILL_COLOR', self.color)
        self.set_property('NODE_WIDTH', self.size)
        self.set_property('NODE_HEIGHT', self.size)

    def set_property(self, property, value):
        '''
        Sets the given property to the given value
        :param node_property: a valid Node property
        :param value: a valid value for the given property
        '''
        if self.view is not None:
            self.view.update_node_views(visual_property=property, values={self.suid: value})

    def set_position(self, xpos, ypos, zpos=0.0):
        '''
        Sets the position of the node to the given x, y and z coordinates
        :param xpos: x-coordinate
        :param ypos: y-coordinate
        :param zpos: z-coordinate (optional)
        '''
        self.xpos = xpos
        self.ypos = ypos
        self.zpos = zpos
        self.set_property('NODE_X_LOCATION', xpos)
        self.set_property('NODE_Y_LOCATION', ypos)
        self.set_property('NODE_Z_LOCATION', zpos)

    def set_label(self, label, size=12, color='white'):
        '''
        Sets the Node label to the given label-string
        :param label: Node label
        :param size: Font size
        :param color: Font color
        '''
        self.set_property('NODE_LABEL', label)
        self.set_property('NODE_LABEL_FONT_SIZE', size)
        self.set_property('NODE_LABEL_COLOR', color)

    def get_suid(self):
        '''
        Returns the SUID for the created edge
        :return: a SUID (int)
        '''
        return self.suid


'''
The following instances of the Node class represent some basic atoms used in the MoleculeFactory.
To change the appearance of these atoms, use the set_property method of the Node object in the
instances below.
'''

class Oxygen(Node):
    '''
    Creates a node that can be used as an Oxygen atom
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'oxygen', 'Circle', 'red', size)
        self.set_label(label)
        self.set_position(xpos, ypos)

class Carbon(Node):
    '''
    Creates a node that can be used as a Carbon atom
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'carbon', 'Circle', 'black', size)
        self.set_label(label)
        self.set_property('NODE_BORDER_PAINT', 'white')
        self.set_position(xpos, ypos)

class Sulfur(Node):
    '''
    Creates a node that can be used as a Sulfur atom
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'sulfur', 'Octagon', 'yellow', size)
        self.set_label(label)
        self.set_property("NODE_LABEL_COLOR", "black")
        self.set_position(xpos, ypos)

class CoenzymeA(Node):
    '''
    Creates a node that can be used as a Coenzyme-A molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'coa', 'Rectangle', 'orange', size)
        self.set_label(label)
        # Make the node a bit wider to fit the longer label
        self.set_property('NODE_WIDTH', size * 2)
        self.set_property('NODE_HEIGHT', size * 2)
        self.set_position(xpos, ypos)

class Ubiquinone(Node):
    '''
    Creates a node that can be used as a Ubiquinol molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'ubiquinone', 'Rectangle', 'brown', size)
        self.set_label(label)
        # Make the node a bit wider to fit the longer label
        self.set_property('NODE_WIDTH', size * 3)
        self.set_property('NODE_HEIGHT', size * 2)
        self.set_property("NODE_LABEL_COLOR", "white")
        self.set_property("NODE_LABEL_FONT_SIZE", size*0.4)
        self.set_position(xpos, ypos)
        
class Hydrogen(Node):
    '''
    Creates a node that can be used as a Hydrogen molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'hydrogen', 'Circle', 'white', size)
        self.set_label(label)
        # Make the node a bit wider to fit the longer label
        self.set_property('NODE_WIDTH', size * 0.5)
        self.set_property('NODE_HEIGHT', size * 0.5)
        self.set_property("NODE_LABEL_FONT_SIZE", size*0.25)
        self.set_property("NODE_LABEL_COLOR", "black")
        self.set_position(xpos, ypos)

class Hydroxide(Oxygen):
    '''
    Creates a node that can be used as a hydroxide group
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Oxygen.__init__(self, network, size, "O", xpos, ypos)
        
        self.offset = 0.75 * size
        self.hydrogen = Hydrogen(network, size, "H", xpos + self.offset, ypos + self.offset)
        network.add_edge(self.suid, self.hydrogen.suid, "bond")
        self.name = "OH"

class Phosphorus(Node):
    '''
    Creates a node that can be used as a phosforus atom
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'phosporus', 'Circle', 'yellow', size)
        self.set_label(label)
        self.set_property('NODE_BORDER_PAINT', 'white')
        self.set_property("NODE_LABEL_COLOR", "black")
        self.set_position(xpos, ypos)

class Guanine(Node):
    '''
    Creates a node that can be used as a guanine molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'guanine', 'Rectangle', 'purple', size)
        self.set_label(label)
        # Make the node a bit wider and higher 
        self.set_property('NODE_WIDTH', size * 2.5)
        self.set_property('NODE_HEIGHT', size * 1.5)
        self.set_position(xpos, ypos)

class Adenine(Node):
    '''
    Creates a node that can be used as a guanine molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'adenine', 'Rectangle', 'yellow', size)
        self.set_label(label)
        self.set_property("NODE_LABEL_COLOR", "black")
        # Make the node a bit wider and higher 
        self.set_property('NODE_WIDTH', size * 2.5)
        self.set_property('NODE_HEIGHT', size * 1.5)
        self.set_position(xpos, ypos)
        
class Ribose(Node):
    '''
    Creates a node that can be used as a robose molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'Ribose', 'Octagon', 'purple', size*1.5)
        self.set_label(label)
        self.set_property("NODE_LABEL_FONT_SIZE", size*0.4)
        self.set_position(xpos, ypos)

class NAD(Node):
    '''
    Creates a node that can be used as a NAD+molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'NAD', 'Rectangle', 'green', size)
        self.set_label(label)
        self.set_property('NODE_WIDTH', size * 2)
        self.set_position(xpos, ypos)

class FAD(Node):
    '''
    Creates a node that can be used as a FAD+molecule
    '''
    def __init__(self, network, size, label, xpos, ypos):
        '''
        Creates the node and sets some basic properties
        :param network: network in which to create the node
        :param size: node-size
        :param label: node-label
        :param xpos: node x-coordinate
        :param ypos: node y-coordinate
        '''
        Node.__init__(self, network, 'FAD', 'Rectangle', 'blue', size)
        self.set_label(label)
        self.set_property('NODE_WIDTH', size * 2)
        self.set_position(xpos, ypos)
